# projet RUMMA #

ce projet vise à mettre en lumiere les differents trajets et agences de voyages routiers, terrestres et maritimes,tout en testant les technologies AdonisJS et Bruno, afin de profiter de leurs outils pour optimiser nos services.

The aim of this project is to highlight the various road, land and sea travel routes and agencies, while testing AdonisJS and Bruno technologies, in order to take advantage of their tools to optimize our services.

## How to install ##

Install my-project
```bash 
git clone
```
    
```bash 
cd votre-projet
```

```bash 
npm install
```

```bash 
cp .env.example .env
```

```bash 
node ace migration:run
```

```bash 
npm run dev
```


## Tech Stack

**Client:** JS, Typescript, 

**Server:** Node, AdonisJS, sqlite


## Contributing

Contributions are always welcome!

See `contributing.md` for ways to get started.

Please adhere to this project's `code of conduct`.