import User from '#models/user'
import { BaseSeeder } from '@adonisjs/lucid/seeders'

export default class extends BaseSeeder {
  async run() {
    await User.createMany([
      {
        first_name: 'Cheifick',
        last_name: 'Patrick',
        phone: '062352105',
        email: 'mayombobrunel@gmail.com',
      },
      {
        first_name: 'Steeve',
        last_name: 'KAFFO',
        phone: '077701373',
        email: 'cheifick44@gmail.com',
      },
    ])
  }
}
